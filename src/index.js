import InputMask from 'inputmask'
import ElementMaskComponent from './components/ElementMask'
import InputMaskComponent from './components/InputMask'
import InputMaskDirective from './directives/InputMask'

const VueInputMaskNg = {
  install(Vue) {
    Vue.component('element-mask', ElementMaskComponent)
    Vue.component('input-mask', InputMaskComponent)
    Vue.directive('mask', InputMaskDirective)

    Vue.prototype.$inputMaskFormat = InputMask.format
    Vue.prototype.$inputMaskIsValid = InputMask.isValid
  }
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VueInputMaskNg)
}

export default VueInputMaskNg