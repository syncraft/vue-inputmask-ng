<p align="center">
  <a href="https://npmcharts.com/compare/vue-inputmask-ng?minimal=true">
     <img src="https://img.shields.io/npm/dm/vue-inputmask-ng.svg" alt="Downloads">
  </a>
  <a href="https://www.npmjs.com/package/vue-inputmask-ng">
    <img src="https://img.shields.io/npm/v/vue-inputmask-ng.svg" alt="Version">
  </a>
  <a href="https://www.npmjs.com/package/vue-inputmask-ng">
    <img src="https://img.shields.io/npm/l/vue-inputmask-ng.svg" alt="License">
  </a>
</p>

# vue-inputmask-ng
### Vue.js plugin which is based on the [InputMask](https://github.com/RobinHerbots/Inputmask)

##### Include vue-inputmask-ng after Vue and it will install itself automatically:
```html
<script src="/path/to/vue.js"></script>
<script src="/path/to/vue-inputmask-ng.umd.js"></script>
```

##### When used with a module system, you must install it via Vue.use():
```
npm install vue-inputmask-ng
```

```javascript
import VueInputMask from 'vue-inputmask-ng'
Vue.use(VueInputMask)
```

##### Usage as directive
```html
<input type="tel" v-mask="'[+9] (999) 999 99 99'"/>
<input type="tel" v-mask="{ mask: '99/99/9999', greedy: true }"/>
<input type="tel" v-mask="{ mask: () => ['999', '(9{4,})'] }">
```

##### Usage as component
```html
<input-mask type="tel" mask="[+9] (999) 999 99 99" @complete="..." @incomplete="..." @cleared="..." />
<element-mask tag="h1" contenteditable="true" mask="[+9] (999) 999 99 99"/>
```

##### Vue instance methods
```javascript
this.$inputMaskFormat('2331973', { alias: 'datetime', inputFormat: 'dd/mm/yyyy' })
this.$inputMaskIsValid('23/03/1973', { alias: 'datetime', inputFormat: 'dd/mm/yyyy' })
```

##### Development
```
npm install
npm run build
```
