import InputMask from 'inputmask'

export default {
  props: {
    tag: {
      type: String,
      required: true
    },
    mask: String,
    placeholder: String
  },

  render(createElement) {
    return createElement(this.tag, this.$slots.default)
  },

  watch: {
    mask: function() {
      this.$el.inputmask.option(this.options)
      this.$el.inputmask.mask(this.$el)
    },
    placeholder: function() {
      this.$el.inputmask.option(this.options)
      this.$el.inputmask.mask(this.$el)
    }
  },

  computed: {
    options: function() {
      return {
        mask: this.mask,
        placeholder: this.placeholder,
        oncomplete: () => this.$emit('complete'),
        onincomplete: () => this.$emit('incomplete'),
        oncleared: () => this.$emit('cleared')
      }
    }
  },

  mounted() { 
    this.$el.inputmask = new InputMask(this.options)
    this.$el.inputmask.mask(this.$el)
  },

  beforeDestroy() {
    if (this.$el.inputmask) {
      this.$el.inputmask.remove()
    }
  }
}