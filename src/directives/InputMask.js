import InputMask from 'inputmask'

export default {
  bind(element, binding) {
    element.inputmask = new InputMask(binding.value)
    element.inputmask.mask(element)
  },

  update(element, binding) {
    if (typeof(binding.value) === 'string') {
      element.inputmask.option({ mask: binding.value })
    } else {
      element.inputmask.option(binding.value)
    }

    element.inputmask.mask(element)
  }
}